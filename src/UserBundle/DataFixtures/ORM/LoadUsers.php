<?php 
    namespace UserBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\FixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use UserBundle\Entity\User;
    use Symfony\Component\DependencyInjection\ContainerAwareInterface;
    use Symfony\Component\DependencyInjection\ContainerInterface;    
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

    class LoadUsers implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
    {
        private $container;


        public function load(ObjectManager $manager)
        {
            $user = new User();
            $user->setUsername('admin');
             $user->setPassword($this->encodePassword($user, '123'));        
            $user->setEmail('admin@mail.com');
            $user->setRoles(array('ROLE_ADMIN'));
            $manager->persist($user);
        
            // the queries aren't done until now
            $manager->flush();
        }
        
        public function setContainer(ContainerInterface $container = null)
        {
            $this->container = $container;
        }
        
        private function encodePassword(User $user, $plainPassword)
        {
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        
            return $encoder->encodePassword($plainPassword, $user->getSalt());
        }
        
        public function getOrder()
        {
            return 10;
        }
    }
?>