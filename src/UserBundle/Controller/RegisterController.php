<?php 
namespace UserBundle\Controller;

use BookBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\RegisterFormType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class RegisterController extends Controller
{
    /**
     * @Template()
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        //$user->setUsername('Leia');
    
        $form = $this->createForm(new RegisterFormType(), $user);
                        
        $form->handleRequest($request);
        if ($form->isValid()) {
    
            $data = $form->getData();
           
            $user->setPassword($this->encodePassword($user, $user->getPlainPassword()));        
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Welcome!')
            ;
            
            $this->authenticateUser($user);
            
            $url = $this->generateUrl('book');

            return $this->redirect($url);
            
           
        }
                        
         return array('form' => $form->createView());
    }
    
    private function encodePassword(User $user, $plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')
            ->getEncoder($user)
        ;
    
        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }
    
    private function authenticateUser(User $user)
    {
        $providerKey = 'secured_area'; // your firewall name
        $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());
    
        $this->getSecurityContext()->setToken($token);
    }
}