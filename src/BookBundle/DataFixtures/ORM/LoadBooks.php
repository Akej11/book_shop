<?php 
    namespace BookBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\FixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use BookBundle\Entity\Book;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

    class LoadBooks implements FixtureInterface, OrderedFixtureInterface
    {
        public function load(ObjectManager $manager)
        {
            $genre1 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Роман');
                             
            $genre2 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Детектив');
                             
            $genre3 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Рассказ');
                             
            $genre4 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Фантастика');
                             
            $genre5 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Приключения');
                             
            $genre6 = $manager->getRepository('BookBundle:Genre')
                             ->findOneByName('Детские');
                             
                             
                             
            $author1 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Фёдор Михайлович Достоевский');
                             
            $author2 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Михаил Афанасьевич Булгаков');
                             
            $author3 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('А.П. Чехов');
                             
            $author4 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Жданова Светлана');
                             
            $author5 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Завойчинская Милена');
                             
            $author6 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Донцова Дарья');
                             
            $author7 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Дюма Александр');
                             
            $author8 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Мазин Александр');
                             
            $author9 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Роулинг Джоан Кэтлин');
                             
            $author10 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Твен Марк');
                             
            $author11 = $manager->getRepository('BookBundle:Author')
                             ->findOneByName('Твен Марк');
                             
            $book1 = new Book();
            $book1->setName('Преступление и наказание.');
            $book1->setGenre($genre1);
            $book1->setAuthor($author1);
            $book1->setDescription('Описание');
            $manager->persist($book1);
            
            $book2 = new Book();
            $book2->setName('Мастер и Маргарита.');
            $book2->setGenre($genre1);
            $book2->setAuthor($author2);
            $book2->setDescription('Описание');
            $manager->persist($book2);
            
            $book3 = new Book();
            $book3->setName('Брак по расчету.');
            $book3->setGenre($genre3);
            $book3->setAuthor($author3);
            $book3->setDescription('Описание');
            $manager->persist($book3);
            
            $book4 = new Book();
            $book4->setName('Невеста Демона.');
            $book4->setGenre($genre4);
            $book4->setAuthor($author4);
            $book4->setDescription('Описание');
            $manager->persist($book4);
            
            $book5 = new Book();
            $book5->setName('Крылья Феникса.');
            $book5->setGenre($genre4);
            $book5->setAuthor($author4);
            $book5->setDescription('Описание');
            $manager->persist($book5);
            
            $book6 = new Book();
            $book6->setName('Тринадцатая невеста');
            $book6->setGenre($genre4);
            $book6->setAuthor($author5);
            $book6->setDescription('Описание');
            $manager->persist($book6);
            
            $book7 = new Book();
            $book7->setName('Гений страшной красоты');
            $book7->setGenre($genre2);
            $book7->setAuthor($author6);
            $book7->setDescription('Описание');
            $manager->persist($book7);
            
            $book8 = new Book();
            $book8->setName('Версаль под хохлому');
            $book8->setGenre($genre2);
            $book8->setAuthor($author6);
            $book8->setDescription('Описание');
            $manager->persist($book8);
            
            $book9 = new Book();
            $book9->setName('Граф Монте-Кристо');
            $book9->setGenre($genre5);
            $book9->setAuthor($author7);
            $book9->setDescription('Описание');
            $manager->persist($book9);
            
            $book10 = new Book();
            $book10->setName('Черный тюльпан');
            $book10->setGenre($genre5);
            $book10->setAuthor($author7);
            $book10->setDescription('Описание');
            $manager->persist($book10);
            
            $book11 = new Book();
            $book11->setName('Белый Волк');
            $book11->setGenre($genre5);
            $book11->setAuthor($author8);
            $book11->setDescription('Описание');
            $manager->persist($book11);
            
            $book12 = new Book();
            $book12->setName('Гарри Поттер и философский камень');
            $book12->setGenre($genre6);
            $book12->setAuthor($author9);
            $book12->setDescription('Описание');
            $manager->persist($book12);
            
            $book13 = new Book();
            $book13->setName('Гарри Поттер и Тайная комната');
            $book13->setGenre($genre6);
            $book13->setAuthor($author9);
            $book13->setDescription('Описание');
            $manager->persist($book13);
            
            $book14 = new Book();
            $book14->setName('Гарри Поттер и узник Азкабана');
            $book14->setGenre($genre6);
            $book14->setAuthor($author9);
            $book14->setDescription('Описание');
            $manager->persist($book14);
            
            $book15 = new Book();
            $book15->setName('Гарри Поттер и Кубок огня');
            $book15->setGenre($genre6);
            $book15->setAuthor($author9);
            $book15->setDescription('Описание');
            $manager->persist($book15);
            
            $book16 = new Book();
            $book16->setName('Гарри Поттер и Дары Cмерти');
            $book16->setGenre($genre6);
            $book16->setAuthor($author9);
            $book16->setDescription('Описание');
            $manager->persist($book16);
            
            $book17 = new Book();
            $book17->setName('Гарри Поттер и Принц-полукровка');
            $book17->setGenre($genre6);
            $book17->setAuthor($author9);
            $book17->setDescription('Описание');
            $manager->persist($book17);
            
            $book18 = new Book();
            $book18->setName('Гарри Поттер и Орден Феникса');
            $book18->setGenre($genre6);
            $book18->setAuthor($author9);
            $book18->setDescription('Описание');
            $manager->persist($book18);
            
            $book19 = new Book();
            $book19->setName('Приключения Тома Сойера');
            $book19->setGenre($genre6);
            $book19->setAuthor($author10);
            $book19->setDescription('Описание');
            $manager->persist($book19);
            
            $book20 = new Book();
            $book20->setName('Пакости в кредит');
            $book20->setGenre($genre2);
            $book20->setAuthor($author10);
            $book20->setDescription('Описание');
            $manager->persist($book20);
            
            $book21 = new Book();
            $book21->setName('Поймать Тень');
            $book21->setGenre($genre4);
            $book21->setAuthor($author4);
            $book21->setDescription('Описание');
            $manager->persist($book21);
            
            $book22 = new Book();
            $book22->setName('Дом на перекрестке');
            $book22->setGenre($genre4);
            $book22->setAuthor($author5);
            $book22->setDescription('Описание');
            $manager->persist($book22);
            
            
        
            // the queries aren't done until now
            $manager->flush();
        }
        
        public function getOrder()
        {
            return 40;
        }
    }
?>