<?php 
    namespace BookBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\FixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use BookBundle\Entity\Genre;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

    class LoadGenre implements FixtureInterface, OrderedFixtureInterface
    {
        public function load(ObjectManager $manager)
        {                 
            $genre1 = new Genre();
            $genre1->setName('Роман');
            $manager->persist($genre1);
            
            $genre2 = new Genre();
            $genre2->setName('Детектив');
            $manager->persist($genre2);
            
            $genre3 = new Genre();
            $genre3->setName('Рассказ');
            $manager->persist($genre3);
            
            $genre4 = new Genre();
            $genre4->setName('Фантастика');
            $manager->persist($genre4);
            
            $genre5 = new Genre();
            $genre5->setName('Приключения');
            $manager->persist($genre5);
            
            $genre6 = new Genre();
            $genre6->setName('Детские');
            $manager->persist($genre6);
        
            // the queries aren't done until now
            $manager->flush();
        }
        
        public function getOrder()
        {
            return 20;
        }
    }
?>