<?php 
    namespace BookBundle\DataFixtures\ORM;

    use Doctrine\Common\DataFixtures\FixtureInterface;
    use Doctrine\Common\Persistence\ObjectManager;
    use BookBundle\Entity\Author;
    use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

    class LoadAuthor implements FixtureInterface, OrderedFixtureInterface
    {
        public function load(ObjectManager $manager)
        {                 
            $author1 = new Author();
            $author1->setName('Фёдор Михайлович Достоевский');
            $manager->persist($author1);
            
            $author2 = new Author();
            $author2->setName('Михаил Афанасьевич Булгаков');
            $manager->persist($author2);
            
            $author3 = new Author();
            $author3->setName('А.П. Чехов');
            $manager->persist($author3);
            
            $author4 = new Author();
            $author4->setName('Жданова Светлана');
            $manager->persist($author4);
            
            $author5 = new Author();
            $author5->setName('Завойчинская Милена');
            $manager->persist($author5);
            
            $author6 = new Author();
            $author6->setName('Донцова Дарья');
            $manager->persist($author6);
            
            $author7 = new Author();
            $author7->setName('Дюма Александр');
            $manager->persist($author7);
            
            $author8 = new Author();
            $author8->setName('Мазин Александр');
            $manager->persist($author8);
            
            $author9 = new Author();
            $author9->setName('Роулинг Джоан Кэтлин');
            $manager->persist($author9);
            
            $author10 = new Author();
            $author10->setName('Твен Марк');
            $manager->persist($author10);
            
            $author11 = new Author();
            $author11->setName('Куликова Галина Михайловна');
            $manager->persist($author11);
            
            // the queries aren't done until now
            $manager->flush();
        }
        
        public function getOrder()
        {
            return 30;
        }
    }
?>