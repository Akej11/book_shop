<?php

namespace BookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('genre' , 'entity' , array(
                      'class'    => 'BookBundle:Genre' ,
                      'property' => 'name' ,
                      'placeholder' => '--Выберите',
                      'empty_data'  => null,
                      'multiple' => false , ))
            ->add('author' , 'entity' , array(
                      'class'    => 'BookBundle:Author' ,
                      'property' => 'name' ,
                      'placeholder' => '--Выберите',
                      'empty_data'  => null,
                      'multiple' => false , ))
            ->add('description')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BookBundle\Entity\Book'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bookbundle_book';
    }
}
