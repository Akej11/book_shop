<?php

namespace BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BookBundle\Entity\Genre;

/**
 * Book
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="BookBundle\Entity\BookRepository")
 */
class Book
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @ORM\ManyToOne(targetEntity="BookBundle\Entity\Genre")
     */
    protected $genre;
    
     /**
     * @ORM\ManyToOne(targetEntity="BookBundle\Entity\Author")
     */
    protected $author;

    


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Book
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getGenre()
    {
        return $this->genre;
    }

    public function setGenre(Genre $genre)
    {
        $this->genre = $genre;
    }
    
    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor(Author $author)
    {
        $this->author = $author;
    }
    
    /**
     * Returns a string name.
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
}

