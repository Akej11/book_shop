<?php

namespace BookBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use BookBundle\Entity\Book;
use BookBundle\Form\BookType;
use BookBundle\Form\SearchBookType;
use BookBundle\Controller\Controller as LocalController;
use Symfony\Component\HttpFoundation\Response;
use Kitpages\DataGridBundle\Grid\GridConfig;
use Kitpages\DataGridBundle\Grid\Field;

/**
 * Book controller.
 *
 * @Route("/book")
 */
class BookController extends LocalController
{
    const PAGE_LIMIT = 20;
    
    /**
     * Lists all Book entities in table.
     *
     * @Route("/admin", name="book_admin")
     * @Method("GET")
     * @Template("BookBundle:Book:admin.html.twig")
     */
    public function indexAdminAction(Request $request)
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
        
        $datatable = $this->get('app.datatable.book');
        $datatable->buildDatatable();
        return array(
           'datatable' => $datatable,
        );
    }
    
    /**
     * Lists all Book entities.
     *
     * @Route("/", name="book")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $form = $this->createSearchForm();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BookBundle:Book')->findAll();
        
        $pagination  = $this->paginatorCreate($entity,$request,self::PAGE_LIMIT);
        return array(
           'pagination' => $pagination,
           'search_form'   => $form->createView(),
        );
    }
    
     /**
     * 
     *
     * @Route("/filter", name="book_filter")
     * @Method("POST")
     * @Template("BookBundle:Book:index.html.twig")
     */
    public function filterAction(Request $request)
    {
        
        $form = $this->createSearchForm();
        $form->handleRequest($request);    
        $em = $this->getDoctrine()->getManager();
    
        $entity = $em->getRepository('BookBundle:Book')->getFilteredBooks($form->getData());
        
        $pagination  = $this->paginatorCreate($entity,$request,self::PAGE_LIMIT);
        return array(
           'pagination' => $pagination,
           'search_form'   => $form->createView(),
        );
    }
    
    /**
     * @Route("/results", name="book_results")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexResultsAction()
    {
        $datatable = $this->get('app.datatable.book');
        $datatable->buildDatatable();
    
        $query = $this->get('sg_datatables.query')->getQueryFrom($datatable);
    
        return $query->getResponse();
    }
    /**
     * Creates a new Book entity.
     *
     * @Route("/", name="book_create")
     * @Method("POST")
     * @Template("BookBundle:Book:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
        
        $entity = new Book();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('book_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    

    private function createSearchForm()
    {
        $form = $this->createForm(new SearchBookType(), null, array(
            'action' => $this->generateUrl('book_filter'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Search')));
        return $form;
    }

    /**
     * Creates a form to create a Book entity.
     *
     * @param Book $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Book $entity)
    {
        $form = $this->createForm(new BookType(), $entity, array(
            'action' => $this->generateUrl('book_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Create')));

        return $form;
    }

    /**
     * Displays a form to create a new Book entity.
     *
     * @Route("/new", name="book_new", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
        
        $entity = new Book();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Book entity.
     *
     * @Route("/{id}", name="book_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BookBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Book entity.
     *
     * @Route("/{id}/edit", name="book_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BookBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }
        

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Book entity.
    *
    * @param Book $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Book $entity)
    {
        $form = $this->createForm(new BookType(), $entity, array(
            'action' => $this->generateUrl('book_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => $this->get('translator')->trans('Update')));

        return $form;
    }
    /**
     * Edits an existing Book entity.
     *
     * @Route("/{id}", name="book_update")
     * @Method("PUT")
     * @Template("BookBundle:Book:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BookBundle:Book')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Book entity.');
        }
        
        

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('book_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Book entity.
     *
     * @Route("/{id}", name="book_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->enforceUserSecurity('ROLE_BOOK_CREATE');
         
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BookBundle:Book')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Book entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('book'));
    }

    /**
     * Creates a form to delete a Book entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('book_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => $this->get('translator')->trans('Delete')))
            ->getForm()
        ;
    }
    
    
}
