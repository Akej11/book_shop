<?php 

namespace BookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use BookBundle\Entity\Book;

class Controller extends BaseController
{
    /**
     * @return \Symfony\Component\Security\Core\SecurityContext
     */
    public function getSecurityContext()
    {
        return $this->container->get('security.context');
    }
    
    protected function enforceUserSecurity($role = 'ROLE_USER')
    {
        if (!$this->getSecurityContext()->isGranted($role)) {
                        
             throw $this->createAccessDeniedException('Need '.$role);

        }
    }
    
    public function paginatorCreate($entity,$request,$limit = 10)
    {
        return $this->get('knp_paginator')
                    ->paginate(
                         $entity, /* query NOT result */
                         $request->query->getInt('page', 1)/*page number*/,
                         $limit/*limit per page*/
                    );
    }
    
}